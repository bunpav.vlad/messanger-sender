<?php
/**
 * Local config for developer of environment.
 *
 */

return [
    'language' => 'en',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:dbname=messenger;host=mysql',
            'username' => 'messenger_user',
            'password' => 'messenger_password',
            'charset' => 'utf8',
        ],

        'queue' => [
            'class' => \yii\queue\redis\Queue::class,
            'channel' => 'base-stack',
            'as log' => \yii\queue\LogBehavior::class,
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
    ],
];
