<?php
declare(strict_types=1);

namespace common\services\tasks\messenger;

use common\services\tasks\contract\MessengerStructureContract;
use DateTime;

/**
 * Trait MessengerStructureTrait
 *
 * Хелпер реализующий MessengerStructureContract
 * @package common\services\tasks\messenger
 */
trait MessengerStructureTrait
{

    /** @var string */
    protected $messengerName;

    /** @var string */
    protected $sendTo;

    /** @var DateTime */
    protected $needSend;

    /** @var string */
    protected $message;

    /**
     * {@inheritdoc}
     */
    public function getMessengerName(): string
    {
        return $this->messengerName;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessengerName(string $messengerName): MessengerStructureContract
    {
        $this->messengerName = $messengerName;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSendTo(): string
    {
        return $this->sendTo;
    }

    /**
     * {@inheritdoc}
     */
    public function setSendTo(string $sendTo): MessengerStructureContract
    {
        $this->sendTo = $sendTo;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage(string $message): MessengerStructureContract
    {
        $this->message = $message;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getNeedSend(): DateTime
    {
        return $this->needSend;
    }

    /**
     * {@inheritdoc}
     */
    public function setNeedSend(DateTime $needSend): MessengerStructureContract
    {
        $this->needSend = $needSend;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function loadStructure(MessengerStructureContract $structure): MessengerStructureContract
    {
        return $this->setSendTo($structure->getSendTo())
            ->setMessengerName($structure->getMessengerName())
            ->setNeedSend($structure->getNeedSend())
            ->setMessage($structure->getMessage());

    }
}