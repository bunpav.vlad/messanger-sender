<?php
declare(strict_types=1);

namespace common\services\tasks\tunnels;

use common\services\tasks\contract\MessageSubscriberContract;
use common\services\tasks\contract\MessengerSenderContract;
use common\services\tasks\contract\MessengerStructureContract;
use common\services\tasks\messenger\MessengerStructureTrait;
use yii\queue\cli\Queue;
use yii\base\BaseObject;
use DateTime;
use function vsprintf;

abstract class AbstractTunnel extends BaseObject implements MessengerSenderContract
{
    use MessengerStructureTrait;

    private const DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * {@inheritdoc}
     */
    public function update(MessageSubscriberContract $subject, MessengerStructureContract $data, ?Queue $queue = null): string
    {

        $this->loadStructure($data);
        return $this->execute($queue);
    }

    /**
     *
     * {@inheritdoc}
     *
     * Выполняет отправку сообщения в мессенджер
     */
    public function execute($queue): string
    {
        return vsprintf("send to messenger: %s\n
                       send to user: %s\n
                       send message: %s\n
                       must be send: %s\n
                       sended: %s",
            [
                $this->messengerName,
                $this->sendTo,
                $this->needSend->format(self::DATE_FORMAT),
                $this->message,
                (new DateTime())->format(self::DATE_FORMAT)
            ]);
    }

}