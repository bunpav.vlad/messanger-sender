<?php
declare(strict_types=1);

namespace common\services\tasks\tunnels;

use common\services\tasks\contract\MessengerStructureContract;
use console\controllers\TelegramPush;

class TelegramTunnel extends AbstractTunnel
{
    public function loadStructure(MessengerStructureContract $structure): MessengerStructureContract
    {
        $structure->setMessengerName('Telegram');
        return parent::loadStructure($structure);

    }

    /**
     * Если установлена очередь, ставит задачу отправить сообщение в Telegram
     *
     * @param \yii\queue\cli\Queue|null $queue
     * @return string
     */
    public function execute($queue): string
    {
        if ($queue !== null) {
            $queue->push(new TelegramPush(
                    $this->getSendTo(),
                    $this->getNeedSend(),
                    $this->getMessage()
                )
            );
        }
        return parent::execute($queue);
    }
}