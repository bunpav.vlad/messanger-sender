<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

use yii\queue\JobInterface;
use yii\queue\cli\Queue;

/**
 * Interface MessengerSenderContract
 *
 * Связующее звено между структурой
 * и выполнением отправки в конкретный мессенджер
 * @package common\services\tasks\contract
 */
interface MessengerSenderContract extends JobInterface, MessengerStructureContract
{
    /**
     * точка входа для выполнения инструкций подписчика
     * @param MessageSubscriberContract $subject
     * @param MessengerStructureContract $data
     * @param null|Queue $queue
     * @return string
     */
    public function update(MessageSubscriberContract $subject, MessengerStructureContract $data, ?Queue $queue = null): string;
}