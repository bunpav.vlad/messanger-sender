<?php
declare(strict_types=1);

namespace common\services\tasks\contract;

/**
 * Interface MessageSubscriberContract
 *
 * Контракт для работы с подписками на отправку сообщений
 * @package common\services\tasks\contract
 */
interface MessageSubscriberContract
{
    /**
     * Добавляет обработчик событий к наблюдателям
     *
     * @param MessengerSenderContract $observer
     * @param int $messengerType
     * @return MessageSubscriberContract
     */
    public function attach(MessengerSenderContract $observer, int $messengerType = 1): MessageSubscriberContract;

    /**
     * Удаляет обработчик событий из наблюдателя
     *
     * @param MessengerSenderContract $observer
     * @param int $messengerType
     * @return MessageSubscriberContract
     */
    public function detach(MessengerSenderContract $observer, int $messengerType = 1): MessageSubscriberContract;

    /**
     * Выполняет зарегистрированные в наблюдатели подпски
     *
     * @param int $messengerType
     * @param MessengerStructureContract $data
     * @return MessengerSenderContract[]
     */
    public function notify(int $messengerType = 0, MessengerStructureContract $data): array;

}