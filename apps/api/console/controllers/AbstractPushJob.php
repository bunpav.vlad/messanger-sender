<?php
declare(strict_types=1);

namespace console\controllers;

use common\services\tasks\contract\MessengerStructureContract;
use common\services\tasks\messenger\MessengerStructureTrait;
use DateTime;
use DateTimeZone;
use function vsprintf;
use yii\queue\JobInterface;

abstract class AbstractPushJob implements JobInterface, MessengerStructureContract
{
    use MessengerStructureTrait;

    private const DATE_FORMAT = 'Y-m-d H:i:s';

    public function __construct(string $sendTo, DateTime $needSend, string $message)
    {
        $this->sendTo = $sendTo;
        $this->needSend = $needSend;
        $this->message = $message;
    }

    /**
     *
     * {@inheritdoc}
     *
     * Выполняет отправку сообщения в мессенджер
     */
    public function execute($queue): string
    {
        $now = new DateTime('now', new DateTimeZone('+0300'));
        if ($now->getTimestamp() < $this->getNeedSend()->getTimestamp()) {
            if ($queue !== null)
                $queue->push($this);
            return 'resend';
        }
        $result = vsprintf(
            "
             in messenger: %s\n
             send to user: %s\n
             send message: %s\n
             must be send: %s\n
             sended: %s\n",
            [
                $this->messengerName,
                $this->sendTo,
                $this->message,
                $this->needSend->format(self::DATE_FORMAT),
                (new DateTime('now', new DateTimeZone('+0300')))->format(self::DATE_FORMAT)
            ]
        );
        /*
        * По хорошему надо было бы пропустить это через дополнительную абстракцию
        * и return $result выводить выше. или просто за логировать вывод
        * тк на этом этапе должна быть реализация отправки в мессенджеры со своими зависимостями
        */
        echo $result;
        return $result;
    }

}