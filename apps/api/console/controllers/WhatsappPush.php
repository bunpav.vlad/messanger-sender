<?php
declare(strict_types=1);

namespace console\controllers;

class WhatsappPush extends AbstractPushJob
{
    protected $messengerName = 'Whatsapp';
}