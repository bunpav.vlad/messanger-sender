<?php
declare(strict_types=1);

namespace console\controllers;

class TelegramPush extends AbstractPushJob
{
    protected $messengerName = 'Telegram';
}