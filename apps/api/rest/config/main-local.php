<?php
/**
 * Local config for developer of environment.
 *
 */

return [
    'components' => [
        'log' => [
            'traceLevel' => 3,
        ],
    ],
];
