<?php

namespace rest\versions\v1\results;

use JsonSerializable;
use function get_object_vars;

/**
 * Class BaseApiResult
 *
 * Базовая структура ответа АПИ
 * @package rest\versions\v1\results
 */
class BaseApiResult implements JsonSerializable
{
    /**
     * @var bool
     */
    protected $success;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * AbstractApiResult constructor.
     * @param bool $success
     * @param string $message
     * @param mixed|null $data
     */
    public function __construct(bool $success, string $message = '', $data = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return BaseApiResult
     */
    public function setSuccess(bool $success): BaseApiResult
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return BaseApiResult
     */
    public function setMessage(string $message): BaseApiResult
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return BaseApiResult
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Преобразование в json структуру
     * @return array
     */
    function jsonSerialize(): array
    {
        return get_object_vars($this);
    }

}