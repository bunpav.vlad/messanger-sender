<?php

namespace rest\versions\v1\controllers;

use common\models\LoginForm;
use rest\versions\v1\results\BaseApiResult;
use yii\rest\Controller;

/**
 * Class UserController
 *
 * Контроллер для авторизации, получения токена
 *
 * @package rest\versions\v1\controllers
 */
class UserController extends Controller
{
    private $user;

    public function init(): void
    {
        parent::init();
        $this->user = \Yii::$app->user;
    }

    /**
     * Метод для получения токена
     *
     * @return BaseApiResult
     */
    public function actionLogin(): BaseApiResult
    {
        $model = new LoginForm();

        if (!$model->load(\Yii::$app->getRequest()->getBodyParams(), '') || !$model->login()) {
            return $this->failValidation($model);
        }

        return new BaseApiResult(true, 'Успешная авторизация', ['token' => $this->user->identity->getAuthKey()]
        );
    }

    /**
     * Выполняет генерацию ошибочного ответа
     *
     * @param LoginForm $form
     * @return BaseApiResult
     */
    private function failValidation(LoginForm $form): BaseApiResult
    {
        $errors = [];
        foreach ($form->getErrors() as $field => $messages) {
            $errors[$field] = $messages;
        }
        return new BaseApiResult(false, 'Не успешная авторизация', $errors);
    }
}
