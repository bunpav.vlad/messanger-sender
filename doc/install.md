# Установка
Докер пробрасывается на локальный порт 80, по этому надо его или освободить или уазать свободный в `docker-compose.yml` файле.

1. поднимаем докер окружение `docker-compose up -d`
2. заходим в контейнер с апи `docker exec -it messenger_api_1 /bin/sh `
3. в контейнере переходим в папку "api" `cd api`
4. Устанавливаем зависимости `composer install`
5. даём права на запись `chmod 777 -R ./rest/runtime && chmod 777 -R ./console/runtime`
6. Выполняем миграции `php yii migrate`
7. Можно выполнить юнит тесты `./vendor/bin/phpunit`
8. запустим демон отправки сообщений `php yii queue/listen`
9. можно запустить не в режиме демона, а пока не вычитает всю очередь `php yii queue/run`

Можно отправлять запросы.
```
curl -XPOST --data "{\"username\":\"demo\", \"password\":\"demo\"}" -H "Content-Type: application/json" http://localhost/v1/user/login
```
полученный токен используем для создания задачи
```
curl -XPOST --data "{\"sendTo\":\"получатель\", \"message\":\"тело сообщения\", \"messengerType\":1, \"dateSend\":\"2018-12-27 10:00\"}" -H "Content-Type: application/json" http://localhost/v1/task/create?access-token=полученный_токен
```
* sendTo - любая строка
* message - любая строка
* messengerType - число от 1 до 7 (маска для отправки в конкретный месенджер или группу)
* dateSend - когда сообщение должно быть отправлено (дата в настоящем или будущем)


для билдинга описательной документации
в корневой папке с файлом `docker-compose.yml`
необходимо выполнить 2 шага:
1. `composer require --dev phpdocumentor/phpdocumentor dev-master`
2. `./vendor/bin/phpdoc -d ./apps/api/ -t ./documentation --title="Документация Мессенжер" --ignore "*/vendor/*"`

в папке `documentation` - будет документация и карта классов проекта
